# PKL di Lingkar9 Tititan Media #
* Author	: Muhamad Sirojudin
* Pertner	: Anwar Sanusi
* School	: SMK Adi Sanggoro
* Created	: 02 Agustus 2017 11:44

# Tentang Repo ini #
* Ini Merupakan Repo yang bersifat publik dan menyimpan hasil dari belajar PHP OOP
* Latihan dimaksudkan untuk persiapan menghadapi project ke-2 di tempat PKL ini (Project Indonesial Idol 2017)

# Aturan Tim #
* Repo disimpan di dalam documentRoot (biasanya htdocs/), di dalam folder team dengan nama folder repo latihanphp-oop
* htdocs/team/latihanphp-oop

# Repo = Repository #