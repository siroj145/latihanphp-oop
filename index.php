<html>
    <head>
        <title>Latihan PHP OOP dengan tutor DuniaIlkom.com</title>
        <style type="text/css">
            .red {
                color: red;
            }
        </style>
    </head>
</html>
<?php
    class Laptop {
        //var $merk;
        private     $pemilik;
        protected   $merk;
        public      $harga;

        //Constructor akan otomatis dipanggil ketika object di-instance
        public function __construct($pemilik = "", $merk = "", $harga = 0) {
            $this->pemilik = $pemilik; //pemilik pada object ini (yang sedang aktif) = $pemilik pada parameter function
            $this->merk    = $merk;
            $this->harga   = $harga;
        }

        private function getHarga(){
            return $this->harga;
        }

        private function getMerk() {
            return $this->merk;
        }

        private function getPemilik() {
            return $this->pemilik;
        }

        public function kendala($kendala) {
            return $kendala;
        }

        //Destructor akan otomatis dipanggil ketika object dihapus (selesai dieksekusi atau di unset atau dapat juga di-set null)
        public function __destruct() {
            echo "Nama Saya ".$this->getPemilik().", punya laptop merk ".$this->getMerk()."<br>harganya ".$this->getHarga().", dengan kendala ".$this->kendala("Keyboard Rusak")." karena keguyur teh dan dipegatin sendiri";    
        }
    }

    //instance object
    $ao756 = new Laptop("Muhamad Sirojudin", "Acer", 1900000);

    //untuk menghapus variable object dan menjalankan destructor dengan paksa, gunakan :
    //unset($ao756); atau
    $ao756 = null;
    echo '<br><br>Object <span class="red">$ao756</span> telah dihancurkan';
?>